class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :employees do |t|
      t.string :name
      t.date :date_of_join
      t.string :designation
      t.string :registration_id

      t.timestamps
    end
  end
end
