Rails.application.routes.draw do
  resources :employees
  resources :teams


  namespace :api do
    namespace :v1 do
      resources :teams
      resources :employees
    end
  end
end
