class Api::V1::EmployeesController < Api::V1::BaseController
  def create
    binding.pry
    employee = Employee.new(employee_params)

    if employee.save
      render json: employee
    else
      render json: {errors: employee.errors.to_sentance}
    end
  end

  def show
    employee = Employee.find(params[:id])
    render json: employee
  end

  def index
    employees = Employee.all
    render json: employees
  end

  private

  def employee_params
    params.require(:employee).permit(:name, :date_of_join, :designation, :registration_id)
  end
end
